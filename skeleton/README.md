# <Board Name>
===================================

* put image here
* short descrition

## Files

* descriptions of key files
* files for assembly/pcb fab should be palced in mfg directory
* manual and pdf of schematic should be placed in "docs" directory
* all source files for production should be included including BOM 

## EDA tools

EAGLE 6.5.0 for Linux - Light Edition

## License

Published under CERN Open Hardware Licence 1.2

## Author

<your name>
